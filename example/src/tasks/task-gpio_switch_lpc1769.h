/*--------------------------------------------------------------------*-

    task-gpio_switch_lpc1769.h (Released 2019-04)

--------------------------------------------------------------------

    - See task-gpio_switch_lpc1769.c for details.

-*--------------------------------------------------------------------*/




#ifndef _GPIO_SWITCH_H
#define _GPIO_SWITCH_H 1


// ------ Public constants -----------------------------------------

#define TEST_GPIO_SWITCH_PORT 1
#define TEST_GPIO_SWITCH_PIN 22

// ------ Public data type -----------------------------------------


// ------ Public function prototypes -------------------------------
void GPIO_SWITCH_Init(void);
void GPIO_SWITCH_Update(void);


#endif


/*------------------------------------------------------------------*-
  ---- END OF FILE -------------------------------------------------
-*------------------------------------------------------------------*/
