/*--------------------------------------------------------------------*-

    task-gpio_led_lpc1769.c (Released 2019-04)

  --------------------------------------------------------------------

    Simple GPIO_LED task for LPC1769.
	[Write Output on LPCXpresso baseboard.]

    Simple led interface code.

-*--------------------------------------------------------------------*/


// Project header
#include "../main/main.h"


// Task header
#include "task-gpio_led_lpc1769.h"


// ------ Public variable ------------------------------------------
extern uint32_t SW_switch_pressed_G;


// ------ Private constants ----------------------------------------
#define TEST_1 (1)	/* Test original task */
#define TEST_3 (3)	/* Test MEF blinking led */
#define TEST_4 (4)	/* Test MEF led w/dimmer */
#define TEST_5 (5)	/* Test modified task # ... */
#define TEST_6 (6)	/* Test modified task # ... */

#define NO_TITILA 0
#define TITILA 1

#define TEST (TEST_5)


// ------ Private variable -----------------------------------------


/*------------------------------------------------------------------*-

    GPIO_LED_Init()

    Prepare for GPIO_LED_Update() function - see below.

-*------------------------------------------------------------------*/
void GPIO_LED_Init(void)
{
	// Set up "GPIO" LED as an output pin
	Chip_IOCON_PinMux(LPC_IOCON, GPIO_LED_PORT, GPIO_LED_PIN, GPIO_LED_PIN_MODE, GPIO_LED_PIN_FUNC);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, GPIO_LED_PORT, GPIO_LED_PIN);
}


#if (TEST == TEST_1)	/* Test original task */
/*------------------------------------------------------------------*-

    GPIO_LED_Update()

    Simple led interface code.

    If SW_switch_pressed_G => SW_NOT_PRESSED, GPIO_LED => LED_OFF
    If SW_switch_pressed_G => SW_PRESSED,     GPIO_LED => LED_ON

    Must schedule every 1 mili Second (soft deadline).

-*------------------------------------------------------------------*/
void GPIO_LED_Update(void)
{
	// Read GPIO_SWITCH "status"
	if (SW_switch_pressed_G == SW_PRESSED)
	{
		// Write GPIO_LED => LED_ON
    	Chip_GPIO_WritePortBit(LPC_GPIO, GPIO_LED_PORT, GPIO_LED_PIN, LED_ON);
	}
	else
	{
		// Write GPIO_LED => LED_OFF
		Chip_GPIO_WritePortBit(LPC_GPIO, GPIO_LED_PORT, GPIO_LED_PIN, LED_OFF);

	}
}
#endif

#if (TEST == TEST_3)

void GPIO_LED_Update(void)
{
	static uint8_t estado= NO_TITILA;
	static uint32_t tiempo_led=0;
	// Read GPIO_SWITCH "status"
	if (SW_switch_pressed_G == SW_PRESSED)
	{
		if(estado==NO_TITILA)
		{
			estado=TITILA;
			// Write GPIO_LED => LED_ON
			tiempo_led++;
			Chip_GPIO_WritePortBit(LPC_GPIO, TEST_GPIO_LED_PORT, TEST_GPIO_LED_PIN, LED_ON);

			if(tiempo_led>700)
				{
					Chip_GPIO_WritePortBit(LPC_GPIO, TEST_GPIO_LED_PORT, TEST_GPIO_LED_PIN, LED_OFF);

				}
			if(tiempo_led>=1000) tiempo_led=0;

		}
	if(estado==TITILA)
		{
		estado=NO_TITILA;
		Chip_GPIO_WritePortBit(LPC_GPIO, TEST_GPIO_LED_PORT, TEST_GPIO_LED_PIN, LED_OFF);
		}

	}
}

#endif

#if (TEST == TEST_4)	/* Test MEF led w/dimmer */
/*------------------------------------------------------------------*-

    GPIO_LED_Update()

    Simple led interface code.

    If SW_switch_pressed_G => SW_NOT_PRESSED, GPIO_LED => LED_OFF
    If SW_switch_pressed_G => SW_PRESSED,     GPIO_LED => LED_ON

    Must schedule every 1 mili Second (soft deadline).

-*------------------------------------------------------------------*/
void GPIO_LED_Update(void)
{
	static uint32_t duty_cycle=0, tiempo_led=0;
	// Read GPIO_SWITCH "status"
	if (SW_switch_pressed_G == SW_PRESSED)
	{
		duty_cycle=duty_cycle+20;
		if(duty_cycle>=100) duty_cycle=0;

		if(duty_cycle)
		{
			tiempo_led++;
			// Write TEST_GPIO_LED => LED_ON

			Chip_GPIO_WritePortBit(LPC_GPIO, TEST_GPIO_LED_PORT, TEST_GPIO_LED_PIN, LED_ON);
			if(tiempo_led> duty_cycle)
				Chip_GPIO_WritePortBit(LPC_GPIO, TEST_GPIO_LED_PORT, TEST_GPIO_LED_PIN, LED_OFF);

			if(tiempo_led>=100) tiempo_led=0;
		}
	}
	else
	{
		// Write GPIO_LED => LED_OFF
		Chip_GPIO_WritePortBit(LPC_GPIO, GPIO_LED_PORT, GPIO_LED_PIN, LED_OFF);

	}
}
#endif

#if (TEST == TEST_5)	/* Test original task */
/*------------------------------------------------------------------*-

    GPIO_LED_Update()

    Simple led interface code.

    If SW_switch_pressed_G => SW_NOT_PRESSED, GPIO_LED => LED_OFF
    If SW_switch_pressed_G => SW_PRESSED,     GPIO_LED => LED_ON

    Must schedule every 1 mili Second (soft deadline).

-*------------------------------------------------------------------*/
void GPIO_LED_Update(void)
{
	uint32_t i;
	// Read GPIO_SWITCH "status"
	if (SW_switch_pressed_G == SW_PRESSED)
	{
		// Write GPIO_LED => LED_ON
    	Chip_GPIO_WritePortBit(LPC_GPIO, GPIO_LED_PORT, GPIO_LED_PIN, LED_ON);


	}
	else
	{
		// Write GPIO_LED => LED_OFF
		Chip_GPIO_WritePortBit(LPC_GPIO, GPIO_LED_PORT, GPIO_LED_PIN, LED_OFF);

	}

	for(i=0;i<100000;i++);
	/*PONEMOS UNA DEMORA BLOQUEANTE, PARA ASEGURARNOS QUE EL WDT PROVOQUE UNA INTERRUPCION.
	 DENTRO DE WATCHDOG_IRQ DEBE CAMBIAR EL ESTADO DE SYSTEM_MODE_G DE NORMAL A FAULT_TASK_TIMING*/
}
#endif
/*------------------------------------------------------------------*-
  ---- END OF FILE -------------------------------------------------
-*------------------------------------------------------------------*/
